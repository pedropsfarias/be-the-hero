const connection = require('../database/connections');

module.exports = {

    async list(request, response) {

        const ong_id = request.headers.auth;

        const ongs = await connection('ongs').where('id', ong_id).select('*');
        return response.json(ongs);

    }

}